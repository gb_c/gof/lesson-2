#include <iostream>
#include "coffee.h"


int main()
{
    Espresso *espresso = new Espresso();
    DarkRoast *darkRoast = new DarkRoast();
    Decorator *chocolate = new ChocolateDecorator(espresso);
    Decorator *cinnamon = new CinnamonDecorator(espresso);
    Decorator *withoutSugar = new WithoutSugarDecorator(darkRoast);

    std::cout << "Cost: " << chocolate->cost() << " Desc: " << chocolate->getDescription() << std::endl;
    std::cout << "Cost: " << cinnamon->cost() << " Desc: " << cinnamon->getDescription() << std::endl;
    std::cout << "Cost: " << withoutSugar->cost() << " Desc: " << withoutSugar->getDescription() << std::endl;
}
