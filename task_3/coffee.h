#ifndef COFFEE_H
#define COFFEE_H

#include <iostream>


class Beverage
{
public:
    virtual std::string getDescription() const = 0;
    virtual double cost() const = 0;
    virtual ~Beverage() {}
};


class HouseBlend: public Beverage
{
public:
    double cost() const override
    {
        return 5.5;
    }

    std::string getDescription() const override
    {
        return "HouseBlend";
    }
};


class DarkRoast: public Beverage
{
public:
    double cost() const override
    {
        return 6.0;
    }

    std::string getDescription() const override
    {
        return "DarkRoast";
    }
};


class Decaf: public Beverage
{
public:
    double cost() const override
    {
        return 7.3;
    }

    std::string getDescription() const override
    {
        return "Decaf";
    }
};


class Espresso: public Beverage
{
public:
    double cost() const override
    {
        return 3.5;
    }

    std::string getDescription() const override
    {
        return "Espresso";
    }
};


class Decorator: public Beverage
{
private:
    Beverage* _beverage;

public:
    Decorator(Beverage *beverage) : _beverage(beverage) {}

    double cost() const override
    {
        return _beverage->cost();
    }

    std::string getDescription() const override
    {
        return _beverage->getDescription();
    }
};


class ChocolateDecorator: public Decorator
{
private:
    const double _cost = 1.6;

public:
    ChocolateDecorator(Beverage *beverage) : Decorator(beverage) {}

    double cost() const override
    {
        return _cost + Decorator::cost();
    }

    std::string getDescription() const override
    {
        return Decorator::getDescription() + " + chocolate";
    }
};


class CinnamonDecorator: public Decorator
{
private:
    const double _cost = 1.2;

public:
    CinnamonDecorator(Beverage *beverage) : Decorator(beverage) {}

    double cost() const override
    {
        return _cost + Decorator::cost();
    }

    std::string getDescription() const override
    {
        return Decorator::getDescription() + " + cinnamon";
    }
};


class WhippedCreamDecorator: public Decorator
{
private:
    const double _cost = 0.9;

public:
    WhippedCreamDecorator(Beverage *beverage) : Decorator(beverage) {}

    double cost() const override
    {
        return _cost + Decorator::cost();
    }

    std::string getDescription() const override
    {
        return Decorator::getDescription() + " + whipped cream";
    }
};


class WithoutSugarDecorator: public Decorator
{
private:
    const double _cost = -0.5;

public:
    WithoutSugarDecorator(Beverage *beverage) : Decorator(beverage) {}

    double cost() const override
    {
        return _cost + Decorator::cost();
    }

    std::string getDescription() const override
    {
        return Decorator::getDescription() + " - sugar";
    }
};

#endif // COFFEE_H
