#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>


class IPizza
{
    public:
    virtual void prepare(std::string) = 0;
    virtual void bake() = 0;
    virtual void cut()  = 0;
    virtual void box()  = 0;
    virtual ~IPizza() {}
};


class CheesePizza : public IPizza
{
private:
    const std::string _name = "Cheese pizza";

public:
    void prepare(std::string str) override
    {
        std::cout << _name << " prepare " << str << std::endl;
    }

    void bake() override
    {
        std::cout << _name << " bake" << std::endl;
    }

    void cut() override
    {
        std::cout << _name << " cut" << std::endl;
    }

    void box() override
    {
        std::cout << _name << " box" << std::endl;
    }
};


class GreekPizza : public IPizza
{
private:
    const std::string _name = "Greek pizza";

public:
    void prepare(std::string str) override
    {
        std::cout << _name << " prepare " << str << std::endl;
    }

    void bake() override
    {
        std::cout << _name << " bake" << std::endl;
    }

    void cut() override
    {
        std::cout << _name << " cut" << std::endl;
    }

    void box() override
    {
        std::cout << _name << " box" << std::endl;
    }
};


class PepperoniPizza : public IPizza
{
private:
    const std::string _name = "Pepperoni pizza";

public:
    void prepare(std::string str) override
    {
        std::cout << _name << " prepare " << str << std::endl;
    }

    void bake() override
    {
        std::cout << _name << " bake" << std::endl;
    }

    void cut() override
    {
        std::cout << _name << " cut" << std::endl;
    }

    void box() override
    {
        std::cout << _name << " box" << std::endl;
    }
};


class IPizzaFactory
{
public:
    virtual IPizza* createPizza() = 0;
    virtual ~IPizzaFactory() {}
};


class CheesePizzaFactory : public IPizzaFactory
{
public:
    IPizza* createPizza()
    {
        return new CheesePizza;
    }
};


class GreekPizzaFactory : public IPizzaFactory
{
public:
    IPizza* createPizza()
    {
        return new GreekPizza;
    }
};


class PepperoniPizzaFactory : public IPizzaFactory
{
public:
    IPizza* createPizza()
    {
        return new PepperoniPizza;
    }
};


struct Pizza
{
    ~Pizza()
    {
        for(const auto & i : vp) delete i;
    }

    void prepare(std::string str)
    {
        for(const auto & i : vp) i->prepare(str);
    }

    void bake()
    {
        for(const auto & i : vp) i->bake();
    }

    void cut()
    {
        for(const auto & i : vp) i->cut();
    }

    void box()
    {
        for(const auto & i : vp) i->box();
    }

    std::vector<IPizza*> vp;
};


class Kitchen
{
public:
    Pizza* orderPizza(IPizzaFactory& factory)
    {
        Pizza* p = new Pizza;
        p->vp.push_back(factory.createPizza());
        return p;
    }
};

#endif // PIZZERIA_H
