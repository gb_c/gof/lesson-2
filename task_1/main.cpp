#include <iostream>
#include "pizzeria.h"


int main()
{
    Kitchen kitchen;
    CheesePizzaFactory cp_factory;
    GreekPizzaFactory gp_factory;
    PepperoniPizzaFactory pp_factory;

    Pizza* pCheesePizza = kitchen.orderPizza(cp_factory);
    Pizza* pGreekPizza = kitchen.orderPizza(gp_factory);
    Pizza* pPepperoniPizza = kitchen.orderPizza(pp_factory);

    std::cout << "Cheese pizza:" << std::endl;
    pCheesePizza->prepare("ingredients");
    std::cout << std::endl << "Greek pizza:" << std::endl;
    pGreekPizza->cut();
    std::cout << std::endl << "Pepperoni pizza:" << std::endl;
    pGreekPizza->box();

    delete pCheesePizza;
    delete pGreekPizza;
    delete pPepperoniPizza;
}
