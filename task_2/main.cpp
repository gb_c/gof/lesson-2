#include <iostream>
#include "light.h"


int main()
{
    Invoker inv;
    std::cout << "Start:" << std::endl;
    inv.getState();

    inv.On();
    inv.changeColor(Colors::GREEN);
    std::cout << "On + Color:" << std::endl;
    inv.getState();

    inv.Back();
    std::cout << "Back:" << std::endl;
    inv.getState();

    inv.changeColor(Colors::CYAN);
    std::cout << "Color:" << std::endl;
    inv.getState();

    inv.Forward();
    std::cout << "Forward:" << std::endl;
    inv.getState();

    inv.Forward();
    std::cout << "Forward:" << std::endl;
    inv.getState();

    inv.Back();
    std::cout << "Back:" << std::endl;
    inv.getState();

    inv.Back();
    std::cout << "Back:" << std::endl;
    inv.getState();

    inv.Back();
    std::cout << "Back:" << std::endl;
    inv.getState();

    inv.Back();
    std::cout << "Back:" << std::endl;
    inv.getState();
}
