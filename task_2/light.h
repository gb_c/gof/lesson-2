#ifndef LIGHT_H
#define LIGHT_H

#include <iostream>
#include <vector>


enum class Colors
{
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    CYAN,
    BLUE,
    VIOLET,
    WHITE
};


class Light
{
private:
    bool state;
    Colors color;

public:
    Light() : state(false), color(Colors::WHITE) {};

    void On()
    {
        if(!state) state = true;
    }

    void Off()
    {
        if(state) state = false;
    }

    void changeColor(Colors newColor)
    {
        color = newColor;
    }

    bool getState() const
    {
        return state;
    }

    Colors getColor() const
    {
        return color;
    }
};


class Command
{
protected:
    Light* _light;

public:
    virtual ~Command() {}
    virtual void Execute() = 0;
    virtual void unExecute() = 0;

    void setLight(Light* light)
    {
        _light = light;
    }
};


class OnCommand : public Command
{
public:
    void Execute() override
    {
        _light->On();
    }

    void unExecute() override
    {
        _light->Off();
    }
};


class OffCommand : public Command
{
public:
    void Execute() override
    {
        _light->Off();
    }

    void unExecute() override
    {
        _light->On();
    }
};


class ChangeColorCommand : public Command
{
private:
    Colors _newColor;
    Colors _currColor;

public:
    ChangeColorCommand(Colors color) : _newColor(color) {}

    void Execute() override
    {
        _currColor = _light->getColor();
        _light->changeColor(_newColor);
    }

    void unExecute() override
    {
        _light->changeColor(_currColor);
    }
};


class Invoker
{
private:
    std::vector<Command*> vCommands;
    Light light;
    Command* pCommand;
    size_t index;

public:
    Invoker() : pCommand(nullptr), index(0) {}

    ~Invoker()
    {
        for(Command* ptr : vCommands)
        {
            delete ptr;
        }
    }

    void On()
    {
        pCommand = new OnCommand();
        pCommand->setLight(&light);
        pCommand->Execute();
        vCommands.insert(vCommands.begin() + index++, pCommand);
    }

    void Off()
    {
        pCommand = new OffCommand();
        pCommand->setLight(&light);
        pCommand->Execute();
        vCommands.insert(vCommands.begin() + index++, pCommand);
    }

    void changeColor(Colors c)
    {
        pCommand = new ChangeColorCommand(c);
        pCommand->setLight(&light);
        pCommand->Execute();
        vCommands.insert(vCommands.begin() + index++, pCommand);
    }

    void Back()
    {
        if(index == 0 || index > vCommands.size())
        {
            std::cout << "There is nothing to back!" << std::endl;
        }
        else
        {
            pCommand = vCommands.at(--index);
            pCommand->unExecute();
        }
    }

    void Forward()
    {
        if(index >= vCommands.size())
        {
            std::cout << "There is nothing to front!" << std::endl;
        }
        else
        {
            pCommand = vCommands.at(index++);
            pCommand->Execute();
        }
    }

    void getState()
    {
        std::cout << std::boolalpha << "State = " << light.getState() << " Color = " << static_cast<int>(light.getColor()) << std::endl;
    }
};

#endif // LIGHT_H
